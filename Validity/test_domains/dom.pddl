(define (domain sokoban-sequential)
  (:requirements :typing)
  (:types thing location direction - object
          player stone - thing)
  (:predicates (clear ?l - location)
	       (at ?t - thing ?l - location)
	       (at-goal ?s - stone)
	       (IS-GOAL ?l - location)
	       (IS-NONGOAL ?l - location)
               (MOVE-DIR ?from ?to - location ?dir - direction))

(:action push-to-goal
:parameters (?dir - direction ?from - location ?p - player ?ppos - location ?s - stone ?to - location)
:precondition
(and
(MOVE-DIR ?from ?to ?dir)
(MOVE-DIR ?ppos ?from ?dir)
(IS-GOAL ?to)
(at ?p ?ppos)
(at ?s ?from)
(clear ?to)
)
:effect
(and
(at ?s ?to)
(at-goal ?s)
(at ?p ?from)
(at ?p ?ppos)
(at ?s ?from)
(clear ?ppos)
(clear ?to)
)
)

(:action push-to-nongoal
:parameters (?dir - direction ?from - location ?p - player ?ppos - location ?s - stone ?to - location)
:precondition
(and
(MOVE-DIR ?from ?to ?dir)
(MOVE-DIR ?ppos ?from ?dir)
(IS-NONGOAL ?to)
(at ?p ?ppos)
(at ?s ?from)
(clear ?to)
)
:effect
(and
(at ?s ?to)
(at-goal ?s)
(at ?p ?from)
(at ?p ?ppos)
(at ?s ?from)
(clear ?ppos)
(clear ?to)
)
)

(:action move
:parameters (?dir - direction ?from - location ?p - player ?to - location)
:precondition
(and
(MOVE-DIR ?from ?to ?dir)
(clear ?to)
(at ?p ?from)
)
:effect
(and
(at ?p ?from)
(clear ?to)
(at ?p ?to)
(clear ?from)
)
)

)