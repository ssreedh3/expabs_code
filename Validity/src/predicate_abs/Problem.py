import sys
import os
import yaml
import pddlpy
import copy
from Search import *
PLANNER_COMMAND = './fdplan.sh {} {}'
VAL_COMMAND = './valplan.sh {} {} {}'
ACTION_DEF_STR = '(:action {}\n:parameters ({})\n:precondition\n(and\n{}\n)\n:effect\n(and\n{}\n)\n)\n'

class Problem:

    def __init__(self, domain_model, problem, foil_prefix, foil_count, lattice_file,
                 dom_templ, prob_templ, search_type, cognitive_bound):
        self.lattice_file = lattice_file
        dom_prob = pddlpy.DomainProblem(domain_model, problem)
        self.cognitive_bound = cognitive_bound
        self.search_type = search_type

        #print ("domain model")
        #print ("init state")
        self.resolved_models = {}
        self.orig_start = self.convert_prop_tuple_list(dom_prob.initialstate())
        #print ("goal state")
        self.goal_state = self.convert_prop_tuple_list(dom_prob.goals())
        self.orig_dom_map = self.convert_domain_obj_map(dom_prob)
        self.concret_costs = {}
        self.generate_lattice_from_file()
        self.foil = []
        for f in range(1,int(foil_count)+1):
            foil_file = foil_prefix + str(f)
            with open(foil_file) as f_fd:
                self.foil.append([i.strip() for i in f_fd.readlines()])
        with open(dom_templ) as d_fd:
            self.domain_template_str = d_fd.read().strip()
        with open(prob_templ) as p_fd:
            self.prob_template_str = p_fd.read().strip()




    def convert_prop_tuple_list(self, orig_prop_list):
        prop_list = set()
        for p in orig_prop_list:
            #print (p)
            if type(p) is tuple:
                prop = ' '.join([str(i).lower() for i in p])
            else:
                prop = ' '.join([str(i).lower() for i in p.predicate])
            #print ("prop",prop)
            prop_list.add(prop)
        return [ prop_list ]

    def convert_domain_obj_map(self, prob_object):
        dom_map = {}
        for act in (prob_object.operators()):
            sorted_var_name = list(prob_object.domain.operators[act].variable_list.keys())
            sorted_var_name.sort()
            para_list = [ k + " - " + prob_object.domain.operators[act].variable_list[k] for k in sorted_var_name]
            action_name = act
            dom_map[action_name] = {}
            dom_map[action_name]['parameters'] = " ".join(para_list) 
            dom_map[action_name]['precondition_pos'] = self.convert_prop_tuple_list(prob_object.domain.operators[act].precondition_pos)
            dom_map[action_name]['precondition_neg'] = self.convert_prop_tuple_list(prob_object.domain.operators[act].precondition_neg)
            dom_map[action_name]['effect_pos'] = self.convert_prop_tuple_list(prob_object.domain.operators[act].effect_pos)
            dom_map[action_name]['effect_neg'] = self.convert_prop_tuple_list(prob_object.domain.operators[act].effect_neg)

        dom_map['problem'] = {}
        dom_map['problem']['init'] = copy.deepcopy(self.orig_start)
        dom_map['problem']['goal'] = copy.deepcopy(self.goal_state)
        dom_map['current_cost'] = 0
        return dom_map

    def ground_all_operators(self, prob_object):
        ground_operators = []
        for act in prob_object.operators():
            ground_operators += list(prob_object.ground_operator(act))
        return ground_operators

    def generate_lattice_from_file(self):
        with open(self.lattice_file, 'r') as l_fd:
            lattice_map = yaml.load(l_fd)
        self.node_list = {}
        self.node_list[lattice_map['Lattice']['Init']] = copy.deepcopy(self.orig_dom_map)
        self.init_node = lattice_map['Lattice']['Init']
        self.edges = {}
        self.inverse_edges = {}
        while len(self.node_list.keys()) < len(lattice_map['Lattice']['Nodes']) + 1:
            for e_raw in lattice_map['Lattice']['Edges']:
                e = [i.lower() for i in e_raw]
                if e[0] in self.node_list.keys() and e[2] not in self.node_list.keys():
                    tmp_copy = copy.deepcopy(self.node_list[e[0]])
                    # Just a stand in for cases with no conditional effects
                    cost_delta = 0
                    for t in tmp_copy.keys():
                        if t != "current_cost" :
                            for k in tmp_copy[t].keys():
                                for s in range(len(tmp_copy[t][k])):
                                    for p in self.node_list[e[0]][t][k][s]:
                                        if e[1] == p.split(' ')[0].lower():
                                            tmp_copy[t][k][s].remove(p)
                                            cost_delta += 1
                    if e[1] not in self.concret_costs.keys():
                        self.concret_costs[e[1]] = cost_delta

                    #print ("tmp_copy",e[1],tmp_copy['problem']['init'])
                    #print ("tmp_copy",e[1],tmp_copy['problem']['goal'])
                    tmp_copy['current_cost'] += cost_delta
                    self.node_list[e[2]] = tmp_copy
        
                if e[0] not in self.edges.keys():
                    self.edges[e[0]] = {} 
                    self.edges[e[0]][e[1]] = e[2] 
                else:
                    #self.edges[e[0]].append((e[1], e[2]))
                    self.edges[e[0]][e[1]] = e[2]
                    #self.inverse_edges[e[2]].append((e[1], e[0]))
                if e[2] not in self.inverse_edges.keys():
                    self.inverse_edges[e[2]] = {}
                    self.inverse_edges[e[2]][e[1]] = e[0]#[(e[1], e[0])]
                else:
                    self.inverse_edges[e[2]][e[1]] = e[0]#[(e[1], e[0])]

    def find_most_abstract_models(self, model):
        if model not in self.edges.keys():
            if self.test_foil_condition_neg(model):
                print ("model",model)
                self.resolved_models[model] = [model]
                return [model]
            else:
                #print ("model",model)
                self.resolved_models[model] = []
                return []
        else:          
            curr_results = []
            for k in self.edges[model].keys():
                if k not in self.resolved_models.keys():
#                    res_models, abs_models = self.find_most_abstract_models(self.edges[model][k])
#                    resolved_models += res_models
#                    curr_results += abs_models
                    curr_results += self.find_most_abstract_models(self.edges[model][k])
                    self.resolved_models[k] = curr_results
            return curr_results

    def find_start_state(self):
        # also include checks for other restrictions
        self.start_state_belief_space = set(self.find_most_abstract_models(self.init_node))

    def make_problem_domain_file(self, curr_model, dom_file, prob_file):
        action_strings = []
        for act in  self.node_list[curr_model].keys():
            if act != "problem" and act !="current_cost":
                precondition_list = ['('+i+')' for i in self.node_list[curr_model][act]['precondition_pos'][0]]
                precondition_list += ['(not ('+i+'))' for i in self.node_list[curr_model][act]['precondition_neg'][0]]
                effect_list = ['('+i+')' for i in self.node_list[curr_model][act]['effect_pos'][0]]
                effect_list += ['(not ('+i+'))' for i in self.node_list[curr_model][act]['effect_neg'][0]]
                action_strings.append(ACTION_DEF_STR.format(act, self.node_list[curr_model][act]['parameters'],"\n".join(precondition_list),"\n".join(effect_list)))
        dom_str = self.domain_template_str.format("\n".join(action_strings))

        goal_state_str_list = ['('+i+')' for i in  self.node_list[curr_model]['problem']['goal'][0]]
        init_state_str_list = ['('+i+')' for i in self.node_list[curr_model]['problem']['init'][0]]
        prob_str = self.prob_template_str.format("\n".join(init_state_str_list), "\n".join(goal_state_str_list))
        
        with open(dom_file, 'w') as d_fd:
            d_fd.write(dom_str)

        with open(prob_file, 'w') as p_fd:
            p_fd.write(prob_str)

    def test_foil_condition_neg(self, curr_model):
        # return true if goal is empty here
        #if len(self.node_list[curr_model]['problem']['goal'][0]) == 0:
        #    return True
        prob_file = "/tmp/prob_greedy.pddl"
        dom_file = "/tmp/dom_greedy.pddl"
        plan_file = "/tmp/plan_greedy.sol"
        self.make_problem_domain_file(curr_model, dom_file, prob_file)
        output_flag = True
        for i in range(len(self.foil)):
            with open(plan_file, 'w') as p_fd:
                p_fd.write("\n".join(self.foil[i]))
            output = [i.strip() for i in os.popen(VAL_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
            if not  eval(output[0]):
                return False
        return True

    def test_foil_condition_pos(self, curr_model):
        # return true if goal is empty here
        #if len(self.node_list[curr_model]['problem']['goal'][0]) == 0:
        #    return True
        prob_file = "/tmp/prob_greedy.pddl"
        dom_file = "/tmp/dom_greedy.pddl"
        plan_file = "/tmp/plan_greedy.sol"
        self.make_problem_domain_file(curr_model, dom_file, prob_file)
        output_flag = True
        for i in range(len(self.foil)):
            with open(plan_file, 'w') as p_fd:
                p_fd.write("\n".join(self.foil[i]))
            output = [i.strip() for i in os.popen(VAL_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
            if  eval(output[0]):
                return False
        return True

    def find_unresolved_foils(self, curr_model, current_foils):
        unreslv_foils = set()
        # return true if goal is empty here
        #if len(self.node_list[curr_model]['problem']['goal'][0]) == 0:
        #    return unreslv_foils
        prob_file = "/tmp/prob_greedy.pddl"
        dom_file = "/tmp/dom_greedy.pddl"
        plan_file = "/tmp/plan_greedy.sol"
        self.make_problem_domain_file(curr_model, dom_file, prob_file)
        for f in current_foils:
            with open(plan_file, 'w') as p_fd:
                p_fd.write("\n".join(f.split('@')))
            output = [i.strip() for i in os.popen(VAL_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
            if eval(output[0]):
                unreslv_foils.add(copy.deepcopy(f))
        return unreslv_foils

    def run_greedy_search(self):
        # Make a node for start state
#        start_node = AbsNode(self, self.start_state_belief_space, [], set(self.foil))
        start_node = HAbsNode(self, self.start_state_belief_space, [], set('@'.join(i) for i in self.foil), exact_map = True)

        #exit(0)
        # Start search
        return GreedySearch(start_node).get_plan()



    def run_blind_conformant(self):
        # Make a node for start state
        start_node = AbsNode(self, self.start_state_belief_space, [],  set('@'.join(i) for i in self.foil))
        #exit(0)
        # Start search
        return astarSearch(start_node).get_plan()

    def run_heuristic_conformant(self):
        # Make a node for start state
        print ("Starting Heuristic search")
        start_node = HAbsNode(self, self.start_state_belief_space, [], set('@'.join(i) for i in self.foil))
        #exit(0)
        # Start search
        return astarSearch(start_node).get_plan()


    def explain(self):
        self.find_start_state()
        print ("Starting state",self.start_state_belief_space)

        if self.search_type.lower() == "greedy":
            # there will be options here
            return self.run_greedy_search()
        elif self.search_type.lower() == "discounted":
            return False
        elif self.search_type.lower() == "heuristics": 
           return self.run_heuristic_conformant()
        else:
           # there will be options here
           return self.run_blind_conformant()



