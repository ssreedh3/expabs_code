#!/bin/bash
domain=${2}
perc=${4}
dest=${5}

pushd ../
python3 /media/data_mount/mycode/EXPABS/src/predicate_abs/Lattice_Generator.py -d ${domain} -m 1 -n ${perc} > ${dest}

for dom in `ls /media/data_mount/mycode/EXPABS_WORKSPACE/ORIGINAL_FILES/TEST_DOMAIN_FILES/|grep ${domain}` #Zeno
do
    #pushd /media/data_mount/mycode/EXPABS/domains/TEST_DOMAINS/${dom}/
    pushd /media/data_mount/mycode/EXPABS_WORKSPACE/ORIGINAL_FILES/TEST_DOMAIN_FILES/${dom}/problem_files/

    for prob_or in `ls|grep -i prob${id}|grep -v template`
    do
        echo "${prob_or}"
        prob=`echo $prob_or|sed 's/.pddl//'`
        mkdir -p lattice_files/set_pred_${foil_count}_${prob}_${perc}/;python3 /media/data_mount/mycode/EXPABS/src/predicate_abs/Lattice_Generator.py -d domain.pddl -p ${prob_or} -m 1 -n ${perc} -t  ./lattice_files/set_pred_${foil_count}_${prob}_${perc}/dom.pddl -q ./lattice_files/set_pred_${foil_count}_${prob}_${perc}/prob.pddl -l ./lattice_files/set_pred_${foil_count}_${prob}_${perc}/lattice.yaml -f ./lattice_files/set_pred_${foil_count}_${prob}_${perc}/foil -r domain_template.pddl -s ${prob}_template.pddl -c ${foil_count} #> /dev/null
        cp domain_template.pddl lattice_files/set_pred_${foil_count}_${prob}_${perc}/
        cp ${prob}_template.pddl lattice_files/set_pred_${foil_count}_${prob}_${perc}/prob_template.pddl
    done
    popd
done
