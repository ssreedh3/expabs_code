# Readme

Code used as part of experiments for 

[1] Hierarchical Expertise-Level Modeling for User Specific Robot-Behavior Explanations.  Sarath Sreedharan, Siddharth Srivastava, Subbarao Kambhampati. IJCAI 2018

[2] Using State Abstractions to Compute Personalized Contrastive Explanations for AI Agent Behavior 
Sarath Sreedharan, Siddharth Srivastava, Subbarao Kambhampati 
Artificial Intelligence Journal 

### Requirements
- [Pddlpy](https://github.com/hfoffani/pddl-lib/blob/master/pddlpy/pddl.py)
- [FF -V 2.3](https://fai.cs.uni-saarland.de/hoffmann/ff.html) Used in lattice generation
- [VAL](https://github.com/KCL-Planning/VAL) Tested on val compiled from commit version ca5565396007eee73ac36527fbf904142b3077c74

The code was tested on Ubuntu 14.04, using python 3.4

### Running the code
Each component comes with a set of test files to run the code on. The driver script in each case is the file Explainer.py and the exact command to run the code on the test files are provided as part of the help message (python Explainer.py -h), given under the usage. Note that for the Cost variant of the cost, to simplify parsing the code actually expects a yaml version of the domain/problem files to be provided (example files part of the test\_domains folder). You can generate test Lattices and foils using the Lattice\_Generator.py script.


Please note that this is a research code and not meant for public distribution or commercial use. You use the code at your own discretion.

If you have any questions feel free to reach out to ssreedh3 at asu edu

