import argparse
import sys
import time
from Problem import *

def main():
    parser = argparse.ArgumentParser(description='''The driver Script for the Explanation generation''',
                                     epilog="Usage >> python3.4 Explainer.py -d ../../test_domains/domain.yaml -p ../../test_domains/prob.yaml -f ../../test_domains/foil -n 1 -l ../../test_domains/lattice.yaml -r ../../test_domains/domain_templ.pddl -s ../../test_domains/prob_templ.pddl -c ../../test_domains/plan")
    '''
        # Flags
        --generate_lattice
    '''

    parser.add_argument('-d', '--domain_model',   type=str, help="Domain file with real PDDL model of robot.", required=True)
    parser.add_argument('-p', '--problem', type=str, help="Problem file for robot.", required=True)
    parser.add_argument('-f', '--foil_file', type=str, help="foil Plan file prefix.", required=True)
    parser.add_argument('-n', '--foil_count', type=str, help="foil count.", required=True)
    parser.add_argument('-l', '--lattice_file', type=str, help="Lattice file (yaml).")
    parser.add_argument('-r', '--domain_templ', type=str, help="Domain template file")
    parser.add_argument('-s', '--prob_templ', type=str, help="Problem template file")
    parser.add_argument('-c', '--current_plan', type=str, help="Current plan")


    if not sys.argv[1:] or '-h' in sys.argv[1:]:
        print (parser.print_help())
        sys.exit(1)
    args = parser.parse_args()
    

    problem = Problem(args.domain_model, args.problem, args.foil_file, args.foil_count, args.lattice_file, args.domain_templ, args.prob_templ, "blind", args.current_plan)
    st_time = time.time()
    pl = problem.explain()
    cost = 0
    #print (problem.concret_costs)
    for p in pl['pred_to_be_explained']:
        cost += problem.concret_costs[p]
    print ("Explanation",pl)
    print ("Cost >>", cost)
    print ("Explanation Size >>>",len(pl['pred_to_be_explained']))
    print ("Total time >>>",time.time() - st_time)

if __name__ == "__main__":
    main()
