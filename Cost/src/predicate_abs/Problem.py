import sys
import os
import yaml
import pddlpy
import copy
from Search import *
PLANNER_COMMAND = './fdplan.sh {} {}'
VAL_COMMAND = './valplan.sh {} {} {}'
FIND_COST = "./find_cost.sh {} {} {}"
VAL_INFO_COMMAND = './val_information.sh {} {} {}'
ACTION_DEF_STR = '(:action {}\n:parameters {}\n:precondition\n(and\n{}\n)\n:effect\n(and\n{}\n)\n)\n'

class Problem:

    def __init__(self, domain_model, problem, foil_prefix, foil_count, lattice_file,
                 dom_templ, prob_templ, search_type, curr_plan):
        self.lattice_file = lattice_file


        dom_prob = {}
        with open(domain_model) as d_fd:
            dom_prob["domain"] = yaml.load(d_fd)
        with open(problem) as p_fd:
            dom_prob["problem"] = yaml.load(p_fd)
        self.orig_dom_map = dom_prob
        self.orig_dom_map['current_cost'] = 0

        self.resolved_models = {}



        self.concret_costs = {}
        self.generate_lattice_from_file()
        self.foil = []

        for f in range(1,int(foil_count)+1):
            foil_file = foil_prefix + str(f)
            with open(foil_file) as f_fd:
                self.foil.append([i.strip() for i in f_fd.readlines()])
        with open(dom_templ) as d_fd:
            self.domain_template_str = d_fd.read().strip()
        with open(prob_templ) as p_fd:
            self.prob_template_str = p_fd.read().strip()
        with open(curr_plan) as r_fd:
            self.orig_plan = [i.strip() for i in r_fd.readlines()]

        self.fact_cost = int(self.find_cost_of_orig_plan(self.orig_plan))






    def generate_lattice_from_file(self):
        with open(self.lattice_file, 'r') as l_fd:
            lattice_map = yaml.load(l_fd)
        self.node_list = {}
        self.node_list[lattice_map['Lattice']['Init']] = copy.deepcopy(self.orig_dom_map)
        self.init_node = lattice_map['Lattice']['Init']
        self.edges = lattice_map['Lattice']['Edge_map']
        self.inverse_edges = lattice_map['Lattice']['Inv_edge_map']
        self.node_pred_map = lattice_map['Lattice']['Node_map']
        self.sup_node =  lattice_map['Lattice']['Sup']
        for nd in lattice_map['Lattice']['Nodes']:
            if nd not in self.node_list:
                self.node_list[nd] = None

    def set_model_for_node(self, node):
        if self.node_list[node]:
            return True
        self.node_list[node] = copy.deepcopy(self.orig_dom_map)
        for pred in  self.node_pred_map[node]:
            cost_delta = 0
            for act in self.node_list[node]["domain"]["actions"]:
                for part in self.node_list[node]["domain"]["actions"][act]:
                    for p1 in self.node_list[node]["domain"]["actions"][act][part]:
                        if "("+pred+" " in p1 or "("+pred+")" in p1:
                            self.node_list[node]["domain"]["actions"][act][part].remove(p1)
                            cost_delta += 1
            for p1 in self.node_list[node]["problem"]["init"]:
                if "("+pred+" " in p1 or "("+pred+")" in p1:
                    self.node_list[node]["problem"]["init"].remove(p1)
                    cost_delta += 1
            for p1 in self.node_list[node]["problem"]["goal"]:
                if "("+pred+" " in p1 or "("+pred+")" in p1:
                    self.node_list[node]["problem"]["goal"].remove(p1)
                    cost_delta += 1

            if pred not in self.concret_costs.keys():
                self.concret_costs[pred] = cost_delta

            #print ("tmp_copy",e[1],tmp_copy['problem']['init'])
            #print ("tmp_copy",e[1],tmp_copy['problem']['goal'])
            self.node_list[node]['current_cost'] += cost_delta



        return True

    def find_most_abstract_models(self):
        model = self.sup_node
        if self.test_foil_condition_neg(model):
            return [model]
        else:
            print ("model",model)
            return []



    def find_start_state(self):
        # also include checks for other restrictions
        self.start_state_belief_space = set(self.find_most_abstract_models())


    def make_problem_domain_file(self, curr_model, dom_file, prob_file):
        self.set_model_for_node(curr_model)
        action_strings = []
        for act in  self.node_list[curr_model]["domain"]["actions"].keys():
            if act != "problem" and act !="current_cost":
                precondition_list = [i for i in self.node_list[curr_model]["domain"]["actions"][act]['precondition']]
                #precondition_list += ['(not ('+i+'))' for i in self.node_list[curr_model]["domain"]["actions"][act]['precondition_neg']]i
                if 'add-effect' in  self.node_list[curr_model]["domain"]["actions"][act]:
                    effect_list = [i for i in self.node_list[curr_model]["domain"]["actions"][act]['add-effect']]
                else:
                    effect_list = []
                if 'del-effect' in self.node_list[curr_model]["domain"]["actions"][act]:
                    effect_list += ['(not '+i+')' for i in self.node_list[curr_model]["domain"]["actions"][act]['del-effect']]
                if 'conditional-effect' in self.node_list[curr_model]["domain"]["actions"][act]:
                    effect_list += ['(when '+i+')' for i in self.node_list[curr_model]["domain"]["actions"][act]['conditional-effect']]
                effect_list += [i for i in self.node_list[curr_model]["domain"]["actions"][act]['metric']]
                action_strings.append(ACTION_DEF_STR.format(act, self.node_list[curr_model]["domain"]["actions"][act]['parameters'],"\n".join(precondition_list),"\n".join(effect_list)))
        dom_str = self.domain_template_str.format("\n".join(action_strings))

        goal_state_str_list = [i for i in  self.node_list[curr_model]['problem']['goal']]
        init_state_str_list = [i for i in self.node_list[curr_model]['problem']['init']]
        prob_str = self.prob_template_str.format("\n".join(init_state_str_list), "\n".join(goal_state_str_list))

        with open(dom_file, 'w') as d_fd:
            d_fd.write(dom_str)

        with open(prob_file, 'w') as p_fd:
            p_fd.write(prob_str)



    def test_validity_of_foils(self, dom_file, prob_file, plan_file):
        output = [i.strip() for i in os.popen(VAL_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
        print ("output",output)
        if not eval(output[0]):
            return False
        cost_of_fact = [i.strip() for i in os.popen(FIND_COST.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
        print ("FC",FIND_COST.format(dom_file, prob_file, plan_file), int(cost_of_fact[0]) > self.fact_cost, int(cost_of_fact[0]))
        if int(cost_of_fact[0]) > self.fact_cost:
            return False
        return True


    def find_cost_of_orig_plan(self, curr_plan):
        tmp_dom = "/tmp/dom.pddl"
        tmp_prob = "/tmp/prob.pddl"
        tmp_plan = "/tmp/plan.sol"
        self.make_problem_domain_file(self.init_node, tmp_dom, tmp_prob)
        with open(tmp_plan, 'w') as p_fd:
            p_fd.write("\n".join(curr_plan))
        print (FIND_COST.format(tmp_dom, tmp_prob, tmp_plan))
        cost_of_plan = [i.strip() for i in os.popen(FIND_COST.format(tmp_dom, tmp_prob, tmp_plan)).read().strip().split('\n')]
        return cost_of_plan[0]


    def test_foil_condition_neg(self, curr_model):
        # return true if goal is empty here
        #if len(self.node_list[curr_model]['problem']['goal'][0]) == 0:
        #    return True
        prob_file = "/tmp/prob.pddl"
        dom_file = "/tmp/dom.pddl"
        plan_file = "/tmp/plan.sol"
        self.make_problem_domain_file(curr_model, dom_file, prob_file)
        output_flag = True
        for i in range(len(self.foil)):
            with open(plan_file, 'w') as p_fd:
                p_fd.write("\n".join(self.foil[i]))
            output = self.test_validity_of_foils(dom_file, prob_file, plan_file) #[i.strip() for i in os.popen(VAL_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
            if not  output:
                return False
        return True

    def test_foil_condition_pos(self, curr_model):
        # return true if goal is empty here
        #if len(self.node_list[curr_model]['problem']['goal'][0]) == 0:
        #    return True
        prob_file = "/tmp/prob.pddl"
        dom_file = "/tmp/dom.pddl"
        plan_file = "/tmp/plan.sol"
        self.make_problem_domain_file(curr_model, dom_file, prob_file)
        output_flag = True
        for i in range(len(self.foil)):
            with open(plan_file, 'w') as p_fd:
                p_fd.write("\n".join(self.foil[i]))
            #output = [i.strip() for i in os.popen(VAL_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
            output = self.test_validity_of_foils(dom_file, prob_file, plan_file)
            if  output:
                return False
        return True

    def find_unresolved_foils(self, curr_model, current_foils):
        unreslv_foils = set()
        # return true if goal is empty here
        #if len(self.node_list[curr_model]['problem']['goal'][0]) == 0:
        #    return unreslv_foils
        prob_file = "/tmp/prob.pddl"
        dom_file = "/tmp/dom.pddl"
        plan_file = "/tmp/plan.sol"
        self.make_problem_domain_file(curr_model, dom_file, prob_file)
        for f in current_foils:
            with open(plan_file, 'w') as p_fd:
                p_fd.write("\n".join(f.split('@')))
            #output = [i.strip() for i in os.popen(VAL_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
            output = self.test_validity_of_foils(dom_file, prob_file, plan_file)
            if output:
                unreslv_foils.add(copy.deepcopy(f))
        return unreslv_foils


    def run_blind_conformant(self):
        # Make a node for start state
        start_node = AbsNode(self, self.start_state_belief_space, [],  set('@'.join(i) for i in self.foil))
        #exit(0)
        # Start search
        exp_node =  BFSSearch(start_node)

        #TODO: Assuming there is a single node
        explanation_map = {}
        explanation_map['pred_to_be_explained'] = exp_node.get_plan()
        #TODO: Find model information
        model_infor = {}
        curr_model = list(exp_node.get_state())[0]
        for act in  self.node_list[curr_model].keys():
            if act != "problem" and act !="current_cost":
                tmp_list = []
                for key in self.node_list[curr_model][act]:
                    for pred in self.node_list[curr_model][act][key]:
                        for orig_pred in explanation_map['pred_to_be_explained']: 
                            if orig_pred in pred:
                                tmp_list += [key] 
                if len(tmp_list) != 0:
                    model_infor[act] = tmp_list

        explanation_map["model_information"] = model_infor
        #TODO: Assuming a single foil
        #prob_file = "/tmp/prob.pddl"
        #dom_file = "/tmp/dom.pddl"
        #plan_file = "/tmp/plan.sol"
        #failure_reasons = []
        #self.make_problem_domain_file(curr_model, dom_file, prob_file)
        #for f in self.foil:
        #    with open(plan_file, 'w') as p_fd:
        #        p_fd.write("\n".join([ac for ac in f])+"\n")
        #    print ()
        #    output = [i.strip() for i in os.popen(VAL_INFO_COMMAND.format(dom_file, prob_file, plan_file)).read().strip().split('\n')]
            #fail_info = output[0]
            #if fail_info.split('@')[0] == "precondition" or fail_info.split('@')[0] == "goal":
        #    explanation_map["failure_cause"] = output #"Action " + fail_info.split('@')[-2] + "failed because the precondition " +fail_info.split('@')[-1]+" is not met"
            
        return explanation_map


    def explain(self):
        self.find_start_state()
        # there will be options here
        return self.run_blind_conformant()



