begin_version
3
end_version
begin_metric
0
end_metric
17
begin_variable
var0
-1
2
Atom at-robby(rooma)
Atom at-robby(roomb)
end_variable
begin_variable
var1
-1
2
Atom free_l(left)
NegatedAtom free_l(left)
end_variable
begin_variable
var2
-1
2
Atom free_l(right)
NegatedAtom free_l(right)
end_variable
begin_variable
var3
-1
2
Atom free_r(left)
NegatedAtom free_r(left)
end_variable
begin_variable
var4
-1
2
Atom free_r(right)
NegatedAtom free_r(right)
end_variable
begin_variable
var5
-1
4
Atom at(ball1, rooma)
Atom at(ball1, roomb)
Atom carry(ball1, left)
Atom carry(ball1, right)
end_variable
begin_variable
var6
-1
4
Atom at(ball10, rooma)
Atom at(ball10, roomb)
Atom carry(ball10, left)
Atom carry(ball10, right)
end_variable
begin_variable
var7
-1
4
Atom at(ball11, rooma)
Atom at(ball11, roomb)
Atom carry(ball11, left)
Atom carry(ball11, right)
end_variable
begin_variable
var8
-1
4
Atom at(ball12, rooma)
Atom at(ball12, roomb)
Atom carry(ball12, left)
Atom carry(ball12, right)
end_variable
begin_variable
var9
-1
4
Atom at(ball2, rooma)
Atom at(ball2, roomb)
Atom carry(ball2, left)
Atom carry(ball2, right)
end_variable
begin_variable
var10
-1
4
Atom at(ball3, rooma)
Atom at(ball3, roomb)
Atom carry(ball3, left)
Atom carry(ball3, right)
end_variable
begin_variable
var11
-1
4
Atom at(ball4, rooma)
Atom at(ball4, roomb)
Atom carry(ball4, left)
Atom carry(ball4, right)
end_variable
begin_variable
var12
-1
4
Atom at(ball5, rooma)
Atom at(ball5, roomb)
Atom carry(ball5, left)
Atom carry(ball5, right)
end_variable
begin_variable
var13
-1
4
Atom at(ball6, rooma)
Atom at(ball6, roomb)
Atom carry(ball6, left)
Atom carry(ball6, right)
end_variable
begin_variable
var14
-1
4
Atom at(ball7, rooma)
Atom at(ball7, roomb)
Atom carry(ball7, left)
Atom carry(ball7, right)
end_variable
begin_variable
var15
-1
4
Atom at(ball8, rooma)
Atom at(ball8, roomb)
Atom carry(ball8, left)
Atom carry(ball8, right)
end_variable
begin_variable
var16
-1
4
Atom at(ball9, rooma)
Atom at(ball9, roomb)
Atom carry(ball9, left)
Atom carry(ball9, right)
end_variable
0
begin_state
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
end_state
begin_goal
12
5 1
6 1
7 1
8 1
9 1
10 1
11 1
12 1
13 1
14 1
15 1
16 1
end_goal
194
begin_operator
drop_l ball1 rooma left
1
0 0
2
0 5 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball1 rooma right
1
0 0
2
0 5 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball1 roomb left
1
0 1
2
0 5 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball1 roomb right
1
0 1
2
0 5 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball10 rooma left
1
0 0
2
0 6 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball10 rooma right
1
0 0
2
0 6 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball10 roomb left
1
0 1
2
0 6 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball10 roomb right
1
0 1
2
0 6 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball11 rooma left
1
0 0
2
0 7 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball11 rooma right
1
0 0
2
0 7 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball11 roomb left
1
0 1
2
0 7 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball11 roomb right
1
0 1
2
0 7 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball12 rooma left
1
0 0
2
0 8 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball12 rooma right
1
0 0
2
0 8 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball12 roomb left
1
0 1
2
0 8 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball12 roomb right
1
0 1
2
0 8 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball2 rooma left
1
0 0
2
0 9 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball2 rooma right
1
0 0
2
0 9 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball2 roomb left
1
0 1
2
0 9 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball2 roomb right
1
0 1
2
0 9 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball3 rooma left
1
0 0
2
0 10 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball3 rooma right
1
0 0
2
0 10 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball3 roomb left
1
0 1
2
0 10 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball3 roomb right
1
0 1
2
0 10 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball4 rooma left
1
0 0
2
0 11 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball4 rooma right
1
0 0
2
0 11 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball4 roomb left
1
0 1
2
0 11 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball4 roomb right
1
0 1
2
0 11 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball5 rooma left
1
0 0
2
0 12 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball5 rooma right
1
0 0
2
0 12 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball5 roomb left
1
0 1
2
0 12 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball5 roomb right
1
0 1
2
0 12 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball6 rooma left
1
0 0
2
0 13 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball6 rooma right
1
0 0
2
0 13 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball6 roomb left
1
0 1
2
0 13 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball6 roomb right
1
0 1
2
0 13 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball7 rooma left
1
0 0
2
0 14 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball7 rooma right
1
0 0
2
0 14 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball7 roomb left
1
0 1
2
0 14 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball7 roomb right
1
0 1
2
0 14 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball8 rooma left
1
0 0
2
0 15 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball8 rooma right
1
0 0
2
0 15 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball8 roomb left
1
0 1
2
0 15 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball8 roomb right
1
0 1
2
0 15 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_l ball9 rooma left
1
0 0
2
0 16 2 0
0 1 -1 0
1
end_operator
begin_operator
drop_l ball9 rooma right
1
0 0
2
0 16 3 0
0 2 -1 0
1
end_operator
begin_operator
drop_l ball9 roomb left
1
0 1
2
0 16 2 1
0 1 -1 0
1
end_operator
begin_operator
drop_l ball9 roomb right
1
0 1
2
0 16 3 1
0 2 -1 0
1
end_operator
begin_operator
drop_r ball1 rooma left
1
0 0
2
0 5 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball1 rooma right
1
0 0
2
0 5 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball1 roomb left
1
0 1
2
0 5 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball1 roomb right
1
0 1
2
0 5 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball10 rooma left
1
0 0
2
0 6 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball10 rooma right
1
0 0
2
0 6 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball10 roomb left
1
0 1
2
0 6 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball10 roomb right
1
0 1
2
0 6 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball11 rooma left
1
0 0
2
0 7 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball11 rooma right
1
0 0
2
0 7 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball11 roomb left
1
0 1
2
0 7 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball11 roomb right
1
0 1
2
0 7 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball12 rooma left
1
0 0
2
0 8 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball12 rooma right
1
0 0
2
0 8 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball12 roomb left
1
0 1
2
0 8 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball12 roomb right
1
0 1
2
0 8 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball2 rooma left
1
0 0
2
0 9 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball2 rooma right
1
0 0
2
0 9 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball2 roomb left
1
0 1
2
0 9 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball2 roomb right
1
0 1
2
0 9 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball3 rooma left
1
0 0
2
0 10 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball3 rooma right
1
0 0
2
0 10 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball3 roomb left
1
0 1
2
0 10 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball3 roomb right
1
0 1
2
0 10 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball4 rooma left
1
0 0
2
0 11 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball4 rooma right
1
0 0
2
0 11 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball4 roomb left
1
0 1
2
0 11 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball4 roomb right
1
0 1
2
0 11 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball5 rooma left
1
0 0
2
0 12 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball5 rooma right
1
0 0
2
0 12 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball5 roomb left
1
0 1
2
0 12 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball5 roomb right
1
0 1
2
0 12 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball6 rooma left
1
0 0
2
0 13 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball6 rooma right
1
0 0
2
0 13 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball6 roomb left
1
0 1
2
0 13 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball6 roomb right
1
0 1
2
0 13 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball7 rooma left
1
0 0
2
0 14 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball7 rooma right
1
0 0
2
0 14 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball7 roomb left
1
0 1
2
0 14 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball7 roomb right
1
0 1
2
0 14 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball8 rooma left
1
0 0
2
0 15 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball8 rooma right
1
0 0
2
0 15 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball8 roomb left
1
0 1
2
0 15 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball8 roomb right
1
0 1
2
0 15 3 1
0 4 -1 0
1
end_operator
begin_operator
drop_r ball9 rooma left
1
0 0
2
0 16 2 0
0 3 -1 0
1
end_operator
begin_operator
drop_r ball9 rooma right
1
0 0
2
0 16 3 0
0 4 -1 0
1
end_operator
begin_operator
drop_r ball9 roomb left
1
0 1
2
0 16 2 1
0 3 -1 0
1
end_operator
begin_operator
drop_r ball9 roomb right
1
0 1
2
0 16 3 1
0 4 -1 0
1
end_operator
begin_operator
move rooma roomb
0
1
0 0 0 1
1
end_operator
begin_operator
move roomb rooma
0
1
0 0 1 0
1
end_operator
begin_operator
pick_l ball1 rooma left
1
0 0
2
0 5 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball1 rooma right
1
0 0
2
0 5 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball1 roomb left
1
0 1
2
0 5 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball1 roomb right
1
0 1
2
0 5 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball10 rooma left
1
0 0
2
0 6 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball10 rooma right
1
0 0
2
0 6 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball10 roomb left
1
0 1
2
0 6 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball10 roomb right
1
0 1
2
0 6 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball11 rooma left
1
0 0
2
0 7 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball11 rooma right
1
0 0
2
0 7 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball11 roomb left
1
0 1
2
0 7 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball11 roomb right
1
0 1
2
0 7 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball12 rooma left
1
0 0
2
0 8 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball12 rooma right
1
0 0
2
0 8 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball12 roomb left
1
0 1
2
0 8 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball12 roomb right
1
0 1
2
0 8 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball2 rooma left
1
0 0
2
0 9 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball2 rooma right
1
0 0
2
0 9 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball2 roomb left
1
0 1
2
0 9 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball2 roomb right
1
0 1
2
0 9 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball3 rooma left
1
0 0
2
0 10 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball3 rooma right
1
0 0
2
0 10 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball3 roomb left
1
0 1
2
0 10 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball3 roomb right
1
0 1
2
0 10 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball4 rooma left
1
0 0
2
0 11 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball4 rooma right
1
0 0
2
0 11 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball4 roomb left
1
0 1
2
0 11 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball4 roomb right
1
0 1
2
0 11 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball5 rooma left
1
0 0
2
0 12 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball5 rooma right
1
0 0
2
0 12 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball5 roomb left
1
0 1
2
0 12 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball5 roomb right
1
0 1
2
0 12 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball6 rooma left
1
0 0
2
0 13 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball6 rooma right
1
0 0
2
0 13 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball6 roomb left
1
0 1
2
0 13 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball6 roomb right
1
0 1
2
0 13 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball7 rooma left
1
0 0
2
0 14 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball7 rooma right
1
0 0
2
0 14 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball7 roomb left
1
0 1
2
0 14 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball7 roomb right
1
0 1
2
0 14 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball8 rooma left
1
0 0
2
0 15 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball8 rooma right
1
0 0
2
0 15 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball8 roomb left
1
0 1
2
0 15 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball8 roomb right
1
0 1
2
0 15 1 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball9 rooma left
1
0 0
2
0 16 0 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball9 rooma right
1
0 0
2
0 16 0 3
0 2 0 1
1
end_operator
begin_operator
pick_l ball9 roomb left
1
0 1
2
0 16 1 2
0 1 0 1
1
end_operator
begin_operator
pick_l ball9 roomb right
1
0 1
2
0 16 1 3
0 2 0 1
1
end_operator
begin_operator
pick_r ball1 rooma left
1
0 0
2
0 5 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball1 rooma right
1
0 0
2
0 5 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball1 roomb left
1
0 1
2
0 5 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball1 roomb right
1
0 1
2
0 5 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball10 rooma left
1
0 0
2
0 6 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball10 rooma right
1
0 0
2
0 6 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball10 roomb left
1
0 1
2
0 6 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball10 roomb right
1
0 1
2
0 6 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball11 rooma left
1
0 0
2
0 7 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball11 rooma right
1
0 0
2
0 7 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball11 roomb left
1
0 1
2
0 7 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball11 roomb right
1
0 1
2
0 7 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball12 rooma left
1
0 0
2
0 8 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball12 rooma right
1
0 0
2
0 8 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball12 roomb left
1
0 1
2
0 8 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball12 roomb right
1
0 1
2
0 8 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball2 rooma left
1
0 0
2
0 9 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball2 rooma right
1
0 0
2
0 9 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball2 roomb left
1
0 1
2
0 9 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball2 roomb right
1
0 1
2
0 9 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball3 rooma left
1
0 0
2
0 10 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball3 rooma right
1
0 0
2
0 10 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball3 roomb left
1
0 1
2
0 10 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball3 roomb right
1
0 1
2
0 10 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball4 rooma left
1
0 0
2
0 11 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball4 rooma right
1
0 0
2
0 11 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball4 roomb left
1
0 1
2
0 11 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball4 roomb right
1
0 1
2
0 11 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball5 rooma left
1
0 0
2
0 12 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball5 rooma right
1
0 0
2
0 12 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball5 roomb left
1
0 1
2
0 12 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball5 roomb right
1
0 1
2
0 12 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball6 rooma left
1
0 0
2
0 13 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball6 rooma right
1
0 0
2
0 13 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball6 roomb left
1
0 1
2
0 13 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball6 roomb right
1
0 1
2
0 13 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball7 rooma left
1
0 0
2
0 14 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball7 rooma right
1
0 0
2
0 14 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball7 roomb left
1
0 1
2
0 14 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball7 roomb right
1
0 1
2
0 14 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball8 rooma left
1
0 0
2
0 15 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball8 rooma right
1
0 0
2
0 15 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball8 roomb left
1
0 1
2
0 15 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball8 roomb right
1
0 1
2
0 15 1 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball9 rooma left
1
0 0
2
0 16 0 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball9 rooma right
1
0 0
2
0 16 0 3
0 4 0 1
1
end_operator
begin_operator
pick_r ball9 roomb left
1
0 1
2
0 16 1 2
0 3 0 1
1
end_operator
begin_operator
pick_r ball9 roomb right
1
0 1
2
0 16 1 3
0 4 0 1
1
end_operator
0
