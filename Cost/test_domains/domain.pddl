(define (domain gripper-strips)
   (:predicates (room ?r)
		(ball ?b)
		(gripper ?g)
		(at-robby ?r)
		(at ?b ?r)
		(free_l ?g)
		(free_r ?g)
		(carry ?o ?g)
		(heavy ?b)
		(light ?b)
		)
   (:functions (total-cost))
   (:action move
       :parameters  (?from ?to)
       :precondition (and  (room ?from) (room ?to) (at-robby ?from))
       :effect (and  (at-robby ?to)
		     (not (at-robby ?from))
                     (increase (total-cost) 1)
		))



   (:action pick_l
       :parameters (?obj ?room ?gripper)
       :precondition  (and  (ball ?obj) (room ?room) (gripper ?gripper)
			    (at ?obj ?room) (at-robby ?room)  (free_l ?gripper))
       :effect (and (carry ?obj ?gripper)
		    (not (at ?obj ?room)) 
		    (not (free_l ?gripper))
		     (when (and (not (free_r ?gripper))) (and (increase (total-cost) 5)))
		     (when (and (not (heavy ?obj))) (and (increase (total-cost) 5)))
                     (increase (total-cost) 1)

))

   (:action pick_r
       :parameters (?obj ?room ?gripper)
       :precondition  (and  (ball ?obj) (room ?room) (gripper ?gripper)
			    (at ?obj ?room) (at-robby ?room) (free_r ?gripper))
       :effect (and (carry ?obj ?gripper)
		    (not (at ?obj ?room)) 
		    (not (free_r ?gripper))
		     (when (and (not (free_l ?gripper))) (and (increase (total-cost) 5)))
		     (when (and (not (heavy ?obj))) (and (increase (total-cost) 5)))
                     (increase (total-cost) 1)

))
   (:action push
       :parameters (?obj ?room ?to-room ?gripper)
       :precondition  (and  (ball ?obj) (room ?room) (gripper ?gripper)
                            (at ?obj ?room) (at-robby ?room) (room ?to-room))
       :effect (and 
                    (not (at ?obj ?room))
                    (not (at-robby ?room))
		    (at ?obj ?to-room)
		    (at-robby ?to-room)
                     (increase (total-cost) 2)

))


   (:action drop_r
       :parameters  (?obj  ?room ?gripper)
       :precondition  (and  (ball ?obj) (room ?room) (gripper ?gripper)
			    (carry ?obj ?gripper) (at-robby ?room))
       :effect (and (at ?obj ?room)
		    (free_r ?gripper)
		    (not (carry ?obj ?gripper))

))


   (:action drop_l
       :parameters  (?obj  ?room ?gripper)
       :precondition  (and  (ball ?obj) (room ?room) (gripper ?gripper)
			    (carry ?obj ?gripper) (at-robby ?room))
       :effect (and (at ?obj ?room)
                    (free_l ?gripper)
		    (not (carry ?obj ?gripper))

))

)
